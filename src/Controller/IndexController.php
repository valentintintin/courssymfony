<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\ProductRepository;
use App\Services\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController {
	private ProductRepository $productRepository;
	private UserService $userService;

	public function __construct(ProductRepository $productRepository, UserService $userService) {
		$this->productRepository = $productRepository;
		$this->userService = $userService;
	}

    #[Route("/", name: "index", methods: ["GET"])]
	public function index(): Response {
	    /** @var User $user */
	    $user = $this->getUser();
	    if ($user) {
		    $this->userService->updateLastLogin($user);
	    }

		return $this->render('index/index.html.twig', [
			'products' => $this->productRepository->getLast(3)
		]);
	}
}
